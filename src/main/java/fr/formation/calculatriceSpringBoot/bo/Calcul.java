package fr.formation.calculatriceSpringBoot.bo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Calcul {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	private Integer a;
	private Integer b;
	private Integer res;

	public Calcul() {
	}

	public Calcul(Integer a, Integer b, Integer res) {
		super();
		this.a = a;
		this.b = b;
		this.res = res;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getA() {
		return a;
	}

	public void setA(Integer a) {
		this.a = a;
	}

	public Integer getB() {
		return b;
	}

	public void setB(Integer b) {
		this.b = b;
	}

	public Integer getRes() {
		return res;
	}

	public void setRes(Integer res) {
		this.res = res;
	}

	@Override
	public String toString() {
		return "Calcul [id=" + id + ", a=" + a + ", b=" + b + ", res=" + res + "]";
	}

}
