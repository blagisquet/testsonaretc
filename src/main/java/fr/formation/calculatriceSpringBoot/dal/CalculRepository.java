package fr.formation.calculatriceSpringBoot.dal;

import org.springframework.data.repository.CrudRepository;

import fr.formation.calculatriceSpringBoot.bo.Calcul;

public interface CalculRepository extends CrudRepository<Calcul, Integer>{

}
