package fr.formation.calculatriceSpringBoot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CalculatriceSpringBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(CalculatriceSpringBootApplication.class, args);
	}

}

