package fr.formation.calculatriceSpringBoot.bll;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.formation.calculatriceSpringBoot.bo.Calcul;
import fr.formation.calculatriceSpringBoot.dal.CalculRepository;

@Service
public class CalculServiceImpl implements CalculService {

	@Autowired
	private CalculRepository dao;
	
	@Override
	public Integer addition(Integer a, Integer b) {
		Calcul c = new Calcul();
		c.setA(a);
		c.setB(b);
		c.setRes(a+b);
		dao.save(c);
		return c.getRes();
	}

}
